set(SRC_FILES
    main.cpp
    Application.cpp
    Camera.cpp
    DebugOutput.cpp
    Mesh.cpp
    ShaderProgram.cpp
    Texture.cpp
)

MAKE_OPENGL_TASK(697Kalinochkin 1 "${SRC_FILES}")
