#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>


float lerp(float a0, float a1, float w)
{
    return (1.0f - w)*a0 + w*a1;
}



class SampleApplication : public Application
{
public:
    MeshPtr _mesh;

    ShaderProgramPtr _shader;

    GLuint _ubo;

    GLuint uniformBlockBinding = 0;

    static const int GRADIENT_SIZE = 20;
    glm::vec2 Gradient[GRADIENT_SIZE][GRADIENT_SIZE];

    static constexpr float HEIGHT_MULTIPLIER = 2;


    void initGradients()
    {
        for(int i = 0; i < GRADIENT_SIZE; ++i)
        {
            for(int j = 0; j < GRADIENT_SIZE; ++j)
            {
                Gradient[i][j] = glm::normalize(glm::vec2(rand(), rand()));
            }
        }
    }

    // Computes the dot product of the distance and gradient vectors.
    float dotGridGradient(int ix, int iy, float x, float y)
    {

        // Precomputed (or otherwise) gradient vectors at each grid node
        // Compute the distance vector
        float dx = x - (float)ix;
        float dy = y - (float)iy;

        // Compute the dot-product
        return (glm::dot(glm::vec2(dx, dy), Gradient[ix][iy]));
    }

    // Compute Perlin noise at coordinates x, y
    float perlin(float x, float y, float &dx, float& dy)
    {

        // Determine grid cell coordinates
        int x0 = (int)x;
        int x1 = x0 + 1;
        int y0 = (int)y;
        int y1 = y0 + 1;

        // Determine interpolation weights
        // Could also use higher order polynomial/s-curve here
        float sx = x - (float)x0;
        float sy = y - (float)y0;

        // Interpolate between grid point gradients
        float n0, n1, ix0, ix1, value;

        n0 = dotGridGradient(x0, y0, x, y);  // d = grad[x0][y0]
        n1 = dotGridGradient(x1, y0, x, y);  // d = grad[x1][y0]
        ix0 = lerp(n0, n1, sx);              // d = lerp(grad[x0][y0], grad[x1][y0], sx)

        n0 = dotGridGradient(x0, y1, x, y);  // d = grad[x0][y1]
        n1 = dotGridGradient(x1, y1, x, y);  // d = grad[x1][y1]
        ix1 = lerp(n0, n1, sx);              // d = lerp(grad[x0][y1], grad[x1][y1], sx)

        value = lerp(ix0, ix1, sy);          // d = lerp(lerp(grad[x0][y0], grad[x1][y0], sx), lerp(grad[x0][y1], grad[x1][y1], sx), sy)

        dx = lerp(lerp(Gradient[x0][y0].x, Gradient[x1][y0].x, sx), lerp(Gradient[x0][y1].x, Gradient[x1][y1].x, sx), sy) * HEIGHT_MULTIPLIER;
        dy = lerp(lerp(Gradient[x0][y0].y, Gradient[x1][y0].y, sx), lerp(Gradient[x0][y1].y, Gradient[x1][y1].y, sx), sy) * HEIGHT_MULTIPLIER;
        return value * HEIGHT_MULTIPLIER;
    }

    MeshPtr makeTerrain(int points)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        for(int i = 0; i < points; ++i)
        {
            for(int j = 0; j < points; ++j)
            {
                float x1 = float(i + 1) / (points + 1) * (GRADIENT_SIZE - 1);
                float x2 = float(i + 2) / (points + 1) * (GRADIENT_SIZE - 1);
                float y1 = float(j + 1) / (points + 1) * (GRADIENT_SIZE - 1);
                float y2 = float(j + 2) / (points + 1) * (GRADIENT_SIZE - 1);

                float dx, dy;

                vertices.push_back(glm::vec3(x1, y1, perlin(x1, y1, dx, dy)));
                normals.push_back(glm::normalize(glm::vec3(-dx, -dy, 1.0f)));
                vertices.push_back(glm::vec3(x1, y2, perlin(x1, y2, dx, dy)));
                normals.push_back(glm::normalize(glm::vec3(-dx, -dy, 1.0f)));
                vertices.push_back(glm::vec3(x2, y1, perlin(x2, y1, dx, dy)));
                normals.push_back(glm::normalize(glm::vec3(-dx, -dy, 1.0f)));

                vertices.push_back(glm::vec3(x2, y2, perlin(x2, y2, dx, dy)));
                normals.push_back(glm::normalize(glm::vec3(-dx, -dy, 1.0f)));
                vertices.push_back(glm::vec3(x1, y2, perlin(x1, y2, dx, dy)));
                normals.push_back(glm::normalize(glm::vec3(-dx, -dy, 1.0f)));
                vertices.push_back(glm::vec3(x2, y1, perlin(x2, y1, dx, dy)));
                normals.push_back(glm::normalize(glm::vec3(-dx, -dy, 1.0f)));

            }
        }


        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }


    void makeScene() override
    {
        Application::makeScene();

        initGradients();

        _mesh = makeTerrain(200);
        _mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-GRADIENT_SIZE / 2.0f, -GRADIENT_SIZE / 2.0f, -0.5f)));

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("697KalinochkinData1/shaderUBO.vert", "697KalinochkinData1/shaderUBO.frag");

        //=========================================================
        //Инициализация Uniform Buffer Object

	    // Выведем размер Uniform block'а.
	    GLint uniformBlockDataSize;
	    glGetActiveUniformBlockiv(_shader->id(), 0, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockDataSize);
	    std::cout << "Uniform block 0 data size = " << uniformBlockDataSize << std::endl;

        if (USE_DSA) {
            glCreateBuffers(1, &_ubo);
            glNamedBufferData(_ubo, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
        }
        else {
	        glGenBuffers(1, &_ubo);
	        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
	        glBufferData(GL_UNIFORM_BUFFER, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
	        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
	    // Привязываем буффер к точке привязки Uniform буферов.
        glBindBufferBase(GL_UNIFORM_BUFFER, uniformBlockBinding, _ubo);


        // Получение информации обо всех uniform-переменных шейдерной программы.
	    if (USE_INTERFACE_QUERY) {
		    GLsizei uniformsCount;
		    GLsizei maxNameLength;
		    glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniformsCount);
		    glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_MAX_NAME_LENGTH, &maxNameLength);
		    std::vector<char> nameBuffer(maxNameLength);

		    std::vector<GLenum> properties = {GL_TYPE, GL_ARRAY_SIZE, GL_OFFSET, GL_BLOCK_INDEX};
		    enum Property {
			    Type,
			    ArraySize,
			    Offset,
			    BlockIndex
		    };
		    for (GLuint uniformIndex = 0; uniformIndex < uniformsCount; uniformIndex++) {
			    std::vector<GLint> params(properties.size());
			    glGetProgramResourceiv(_shader->id(), GL_UNIFORM, uniformIndex, properties.size(), properties.data(),
					    params.size(), nullptr, params.data());
			    GLsizei realNameLength;
			    glGetProgramResourceName(_shader->id(), GL_UNIFORM, uniformIndex, maxNameLength, &realNameLength, nameBuffer.data());

			    std::string uniformName = std::string(nameBuffer.data(), realNameLength);

			    std::cout << "Uniform " << "index = " << uniformIndex << ", name = " << uniformName << ", block = " << params[BlockIndex] << ", offset = " << params[Offset] << ", array size = " << params[ArraySize] << ", type = " << params[Type] << std::endl;
		    }
	    }
	    else {
		    GLsizei uniformsCount;
		    glGetProgramiv(_shader->id(), GL_ACTIVE_UNIFORMS, &uniformsCount);

		    std::vector<GLuint> uniformIndices(uniformsCount);
		    for (int i = 0; i < uniformsCount; i++)
			    uniformIndices[i] = i;
		    std::vector<GLint> uniformBlocks(uniformsCount);
		    std::vector<GLint> uniformNameLengths(uniformsCount);
		    std::vector<GLint> uniformTypes(uniformsCount);
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_BLOCK_INDEX, uniformBlocks.data());
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_NAME_LENGTH, uniformNameLengths.data());
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_TYPE, uniformTypes.data());

		    for (int i = 0; i < uniformsCount; i++) {
			    std::vector<char> name(uniformNameLengths[i]);
			    GLsizei writtenLength;
			    glGetActiveUniformName(_shader->id(), uniformIndices[i], name.size(), &writtenLength, name.data());
			    std::string uniformName = name.data();

			    std::cout << "Uniform " << "index = " << uniformIndices[i] << ", name = " << uniformName << ", block = " << uniformBlocks[i] << ", type = " << uniformTypes[i] << std::endl;
		    }
	    }
    }

    void update() override
    {
        Application::update();

        //Обновляем содержимое Uniform Buffer Object

#if 0
        //Вариант с glMapBuffer
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        GLvoid* p = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
        memcpy(p, &_camera, sizeof(_camera));
        glUnmapBuffer(GL_UNIFORM_BUFFER);
#elif 0
        //Вариант с glBufferSubData
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(_camera), &_camera);
#else
        //Вариант для буферов, у которых layout отличается от std140

        //Имена юниформ-переменных
        const char* names[2] =
        {
            "viewMatrix",
            "projectionMatrix"
        };

        GLuint index[2];
        GLint offset[2];

        //Запрашиваем индексы 2х юниформ-переменных
        glGetUniformIndices(_shader->id(), 2, names, index);

        //Зная индексы, запрашиваем сдвиги для 2х юниформ-переменных
        glGetActiveUniformsiv(_shader->id(), 2, index, GL_UNIFORM_OFFSET, offset);

	    // Вывод оффсетов.
	    static bool hasOutputOffset = false;
	    if (!hasOutputOffset) {
		    std::cout << "Offsets: viewMatrix " << offset[0] << ", projMatrix " << offset[1] << std::endl;
		    hasOutputOffset = true;
	    }

        //Устанавливаем значения 2х юниформ-перменных по отдельности
	    if (USE_DSA) {
		    glNamedBufferSubData(_ubo, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
		    glNamedBufferSubData(_ubo, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
	    }
	    else {
		    glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
		    glBufferSubData(GL_UNIFORM_BUFFER, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
		    glBufferSubData(GL_UNIFORM_BUFFER, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
	    }
#endif
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        unsigned int blockIndex = glGetUniformBlockIndex(_shader->id(), "Matrices");
        glUniformBlockBinding(_shader->id(), blockIndex, uniformBlockBinding);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
        _mesh->draw();

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
